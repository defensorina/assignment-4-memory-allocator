#define _DEFAULT_SOURCE

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "mem_internals.h"
#include "mem.h"
#include "util.h"


#define NO_FLAGS 0

void debug_block(struct block_header* b, const char* fmt, ... );
void debug(const char* fmt, ... );

extern inline block_size size_from_capacity( block_capacity cap );
extern inline block_capacity capacity_from_size( block_size sz );

static bool            block_is_big_enough( size_t query, struct block_header* block ) { return block->capacity.bytes >= query; }
static size_t          pages_count   ( size_t mem )                      { return mem / getpagesize() + ((mem % getpagesize()) > 0); }
static size_t          round_pages   ( size_t mem )                      { return getpagesize() * pages_count( mem ) ; }

static void block_init( void* restrict addr, block_size block_sz, void* restrict next ) {
  *((struct block_header*)addr) = (struct block_header) {
    .next = next,
    .capacity = capacity_from_size(block_sz),
    .is_free = true
  };
}

static size_t region_actual_size( size_t query ) {return size_max( round_pages( query ), REGION_MIN_SIZE ); }

extern inline bool region_is_invalid( const struct region* r );

static void* map_pages(void const* addr, size_t length, int additional_flags) {
  return mmap((void*) addr,length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | additional_flags,-1, 0);
}

static struct region alloc_region(void const * addr, size_t query) {
    size_t region_size = region_actual_size(size_from_capacity((block_capacity){query}).bytes );
    void* address = map_pages(addr, region_size, MAP_FIXED_NOREPLACE);

    if (address == MAP_FAILED)
        address = map_pages(addr, region_size, NO_FLAGS);
    
    if (address == MAP_FAILED)
        return REGION_INVALID;
    
    struct region result_object = {address, region_size, address==addr};
    block_init(address, (block_size) {result_object.size}, NULL);
    return result_object;
}

static void* block_after( struct block_header const* block );

void* heap_init( size_t initial ) {
  const struct region region = alloc_region( HEAP_START, initial );
  if ( region_is_invalid(&region) ) return NULL;

  return region.addr;
}

#define BLOCK_MIN_CAPACITY 24

static bool block_splittable( struct block_header* restrict block, size_t query) {
  return block-> is_free && query + offsetof( struct block_header, contents ) + BLOCK_MIN_CAPACITY <= block->capacity.bytes;
}

static bool split_if_too_big(struct block_header* block, size_t query) {
    if (block!=NULL && block_splittable(block, query)){
      void* split_at = block->contents + query;
      block_init(split_at, (block_size) {block->capacity.bytes - query }, block->next);
      block->next = split_at;
      block->capacity.bytes = query;
      return true;
    }
    return false;
}

static void* block_after(struct block_header const* block)              {
  return  (void*) (block->contents + block->capacity.bytes);
}
static bool blocks_continuous(struct block_header const* fst, struct block_header const* snd ) {
  return (void*) snd == block_after(fst);
}

static bool mergeable(struct block_header const* restrict fst, struct block_header const* restrict snd) {
  return fst->is_free && snd->is_free && blocks_continuous( fst, snd ) ;
}

static bool try_merge_with_next(struct block_header* block) {
    if(block->next == NULL || !mergeable(block, block->next))
        return false;

    struct block_header *new = block->next;
    block->next = new->next;
    block->capacity.bytes += size_from_capacity(new->capacity).bytes;
    return true;
}

struct block_search_result {
  enum {BSR_FOUND_GOOD_BLOCK, BSR_REACHED_END_NOT_FOUND, BSR_CORRUPTED} type;
  struct block_header* block;
};


static struct block_search_result find_good_or_last (struct block_header* restrict block, size_t sz)    {
    if (block == NULL)
        return (struct block_search_result) {BSR_CORRUPTED, NULL};

    struct block_header* i;
    for (i = block; i != NULL; i = i->next) {
        if (i->is_free) {
            while(try_merge_with_next(i));

            if (block_is_big_enough(sz, i))
                return (struct block_search_result) {BSR_FOUND_GOOD_BLOCK, i};
        }
        if (i->next == NULL)
            break;
    }
    return (struct block_search_result) {BSR_REACHED_END_NOT_FOUND, i};
}

static struct block_search_result try_memalloc_existing(size_t query, struct block_header* block) {
    struct block_search_result searched_block = find_good_or_last(block, query);
    if (searched_block.type == BSR_FOUND_GOOD_BLOCK) {
        split_if_too_big(searched_block.block, query);
        searched_block.block->is_free = false;
    }
    return searched_block;
}



static struct block_header* grow_heap(struct block_header* restrict last, size_t query) {
    if (last == NULL) return NULL;
    struct region new_result_object = alloc_region(last->contents + last->capacity.bytes, query);
    if (region_is_invalid(&new_result_object) || !new_result_object.extends || !last->is_free) {
      last->next = new_result_object.addr;
      return new_result_object.addr;
    }
    last->capacity.bytes += new_result_object.size;
    return last;
}

static struct block_header* memalloc(size_t query, struct block_header* heap_start) {
    size_t n_query = size_max(query, BLOCK_MIN_CAPACITY);
    struct block_search_result block = try_memalloc_existing(n_query, heap_start);

    if(block.type == BSR_CORRUPTED)
        return NULL;
      
    if (block.type == BSR_FOUND_GOOD_BLOCK){
        block.block->is_free = false;
        return block.block;
    }

    if (block.type != BSR_REACHED_END_NOT_FOUND || block.block == NULL) return NULL;
    
    block.block = grow_heap(block.block, n_query);
    block = try_memalloc_existing(n_query, block.block);
    
    if(block.block != NULL) block.block->is_free = false;
    
    return block.block;
}

void* _malloc(size_t query) {
  struct block_header* const addr = memalloc(query, (struct block_header*) HEAP_START);
  if (addr) return addr->contents;
  else return NULL;
}

static struct block_header* block_get_header(void* contents) {
  return (struct block_header*) (((uint8_t*)contents)-offsetof(struct block_header, contents));
}

void _free( void* mem ) {
    if (!mem) return;
    struct block_header* header = block_get_header(mem);
    header->is_free = true;
    bool is_merged = true;
    while(try_merge_with_next(header));
}
